package fr.univ_littoral.calc;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TouchPanel extends JPanel {	
	
	public TouchPanel(JPanel parent) {
		this.setPreferredSize(parent.getPreferredSize());
		buildWidgets();
	}
	
	private void buildWidgets() {
		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = gbc.gridy = 0;
		
		add(new JButton("1"), gbc);		
		
		
	}
}
