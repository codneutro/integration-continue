package fr.univ_littoral.calc;

import javax.swing.JFrame;


@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	
	public MainFrame() {
		super("Calculatrice");
		initWindowProperties();
	}
	
	private void initWindowProperties() {
		setSize(300, 400);
		setLocationRelativeTo(null);
		setContentPane(new MainPanel(this));
		pack();
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}