package fr.univ_littoral.calc;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {
	
	private JTextField screenField;
	private Dimension parentSize;
	
	
	public MainPanel(JFrame parent) {
		parentSize = parent.getSize();
		setPreferredSize(parentSize);
		buildUI();
	}
	
	private void buildWidgets() {
		screenField = new JTextField("0");
		screenField.setEnabled(false);
		screenField.setHorizontalAlignment(JTextField.RIGHT);
		screenField.setPreferredSize(new Dimension(parentSize.width - 20, 20));
	}
	
	private void setupLayout() {
		this.add(screenField, BorderLayout.NORTH);
		this.add(new TouchPanel(this), BorderLayout.CENTER);
	}
	
	private void buildUI() {
		buildWidgets();
		setupLayout();
	}
}
